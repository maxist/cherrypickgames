﻿using UnityEngine;
using System.Collections;

public class Column : MonoBehaviour
{
    private bool isUp = true;
    public float maxY = 4;
    public float minY = 0;
    public static float speed = 5;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.GetComponent<Bird>() != null)
        {
            //If the bird hits the trigger collider in between the columns then
            //tell the game control that the bird scored.
            GameControl.instance.BirdScored();
        }
    }

    private void Update()
    {
        if (transform.position.y >= maxY) isUp = false;
        if (transform.position.y <= minY) isUp = true;

        if (isUp)
            transform.Translate(Vector3.up * Time.deltaTime * speed, Space.World);
        else
            transform.Translate(Vector3.down * Time.deltaTime * speed, Space.World);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class DifficultyLevel  {

    public static void Easy(int HPNumber)
    {
        Column.speed = 0.5F;
        PlayerStats.HP = HPNumber;
    }

    public static void Medium(int HPNumber)
    {
        Column.speed = 2F;
        PlayerStats.HP = HPNumber;
    }

    public static void Hard(int HPNumber)
    {
        Column.speed = 5F;
        PlayerStats.HP = HPNumber;
    }
}

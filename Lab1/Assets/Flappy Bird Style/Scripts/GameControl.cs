﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class GameControl : MonoBehaviour
{
    public static GameControl instance;         //A reference to our game control script so we can access it statically.
    public Text scoreText;                      //A reference to the UI text component that displays the player's score.
    public GameObject gameOvertext;				//A reference to the object that displays the text which appears when the player dies.
    public Text HPText;


    //private int score = 0;						//The player's score.
    public bool isLoseHP = false;
    public bool gameOver = false;				//Is the game over?
    public bool isgameRunning = true;
    public float scrollSpeed = -1.5f;
    private Text loseHpText;
    
    void Awake()
    {
        //If we don't currently have a game control...
        if (instance == null)
            //...set this one to be it...
            instance = this;
        //...otherwise...
        else if (instance != this)
            //...destroy this one because it is a duplicate.
            Destroy(gameObject);

        HPText.text = "HP: " + PlayerStats.HP;
        scoreText.text = "Score: " + PlayerStats.Score;
        loseHpText = gameOvertext.GetComponentInChildren<Text>();
    }

    void Update()
    {
        Debug.Log("gameover: " + gameOver);
        Debug.Log(HPText.text);
        //If the game is over and the player has pressed some input...
        if (gameOver && Input.GetMouseButtonDown(0))
        {
            //...reload the current scene.
            SceneManager.LoadScene("MainManu");
        }

        if (isLoseHP && Input.GetMouseButtonDown(0))
        {
            //...reload the current scene.
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }

    public void BirdScored()
    {
        //The bird can't score if the game is over.
        if (gameOver)
            return;

        if (isLoseHP)
            return;
        //If the game is not over, increase the score...
        PlayerStats.Score++;
        //...and adjust the score text.
        scoreText.text = "Score: " + PlayerStats.Score.ToString();
    }

    public void BirdDied()
    {
        if (PlayerStats.HP == 1)
        {
            //Activate the game over text.
            gameOvertext.SetActive(true);
            loseHpText.text = "Game over";
            //Set the game to be over.
            gameOver = true;
            isgameRunning = false;
            ResetStatsPlayer();
        }
        else
        {
            isgameRunning = false;
            isLoseHP = true;
            PlayerStats.HP--;
            HPText.text = "HP: " + PlayerStats.HP.ToString();
            gameOvertext.SetActive(true);
            loseHpText.text = "You lost your life, your points will be continued";
        }
    }

    private void ResetStatsPlayer()
    {
        PlayerStats.HP = 3;
        PlayerStats.Score = 0;
    }
}

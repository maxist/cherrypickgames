﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadOnClick : MonoBehaviour
{

    public GameObject loadingImage;
    public Dropdown dropdown;
    public Dropdown HPdropdown;

    private void Awake()
    {
        dropdown.options.Clear();
        dropdown.options.Add(new Dropdown.OptionData() { text = "Ease" });
        dropdown.options.Add(new Dropdown.OptionData() { text = "Medium" });
        dropdown.options.Add(new Dropdown.OptionData() { text = "Hard" });

        HPdropdown.options.Clear();
        HPdropdown.options.Add(new Dropdown.OptionData() { text = "3" });
        HPdropdown.options.Add(new Dropdown.OptionData() { text = "4" });
        HPdropdown.options.Add(new Dropdown.OptionData() { text = "5" });
    }

    public void LoadScene(int level)
    {
        int selectedIndex = dropdown.value;
        int numberOfHP;
        if (int.TryParse(HPdropdown.options[HPdropdown.value].text, out numberOfHP))
        {
            switch (selectedIndex)
            {
                case 0:
                    DifficultyLevel.Easy(numberOfHP);
                    break;
                case 1:
                    DifficultyLevel.Medium(numberOfHP);
                    break;
                case 2:
                    DifficultyLevel.Hard(numberOfHP);
                    break;
                default:
                    DifficultyLevel.Easy(numberOfHP);
                    break;
            }
            SceneManager.LoadScene("Main");
        }
        else
        {
            Debug.LogError("Chosen option is no number: " + HPdropdown.options[selectedIndex].text);
            // or throw an exception or whatever
        }
    }


}

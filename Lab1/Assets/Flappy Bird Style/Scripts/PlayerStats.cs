﻿public static class PlayerStats {

    private static int hp = 3;

    public static int HP
    {
        get { return hp; }
        set { hp = value; }
    }

    private static int score = 0;

    public static int Score
    {
        get { return score; }
        set { score = value; }
    }

}

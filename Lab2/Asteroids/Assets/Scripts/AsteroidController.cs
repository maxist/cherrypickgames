﻿using UnityEngine;

public class AsteroidController : MonoBehaviour
{
    public GameObject smallAsteroid;
    public float minSpeedAsteroid = -50.0f;
    public float maxSpeedAsteroid = 150.0f;

    private GameController gameController;

    void Start()
    {
        gameController =
            GameController.instance;

        GetComponent<Rigidbody2D>()
            .AddForce(transform.up * Random.Range(minSpeedAsteroid, maxSpeedAsteroid));

        GetComponent<Rigidbody2D>()
            .angularVelocity = Random.Range(-0.0f, 90.0f);
    }


    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag.Equals("Bullet"))
        {
            Destroy(collision.gameObject);

            if (tag.Equals("Large Asteroid"))
            {
                Instantiate(smallAsteroid,
                    new Vector3(transform.position.x - .5f,
                        transform.position.y - .5f, 0),
                        Quaternion.Euler(0, 0, 90));

                Instantiate(smallAsteroid,
                    new Vector3(transform.position.x + .5f,
                        transform.position.y + .0f, 0),
                        Quaternion.Euler(0, 0, 0));

                Instantiate(smallAsteroid,
                    new Vector3(transform.position.x + .5f,
                        transform.position.y - .5f, 0),
                        Quaternion.Euler(0, 0, 270));

                gameController.SplitAsteroid();

            }
            else
                gameController.DecrementAsteroids();

            gameController.IncrementScore();

            Destroy(gameObject);
        }
    }
}
﻿using UnityEngine;

public class BulletController : MonoBehaviour {

    public int ForceBullet = 400;

    void Start()
    {
        Destroy(gameObject, 1.0f);

        GetComponent<Rigidbody2D>()
            .AddForce(transform.up * ForceBullet);
    }
}

﻿using UnityEngine;

public class MoveInCamera : MonoBehaviour
{
    void Update()
    {
        if (transform.position.x > 9)
        {

            transform.position = new Vector3(-9, transform.position.y, 0);

        }
        else if (transform.position.x < -9)
        {
            transform.position = new Vector3(9, transform.position.y, 0);
        }

        else if (transform.position.y > 5)
        {
            transform.position = new Vector3(transform.position.x, -5, 0);
        }

        else if (transform.position.y < -5)
        {
            transform.position = new Vector3(transform.position.x, 5, 0);
        }
    }
}

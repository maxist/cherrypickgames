﻿using UnityEngine;

public class ShipController : MonoBehaviour {

    float rotationSpeed = 100.0f;
    float thrustForce = 3f;

    public GameObject bullet;

    private GameController gameController;

    void Start()
    {
        gameController =
            GameController.instance;
    }

    void FixedUpdate()
    {
        // Rotate the ship if necessary
        transform.Rotate(0, 0, -Input.GetAxis("Horizontal") *
            rotationSpeed * Time.deltaTime);

        // Thrust the ship if necessary
        GetComponent<Rigidbody2D>().
            AddForce(transform.up * thrustForce *
                Input.GetAxis("Vertical"));

        // Has a bullet been fired
        if (Input.GetMouseButtonDown(0))
            ShootBullet();
    }

    void OnTriggerEnter2D(Collider2D c)
    {
        // Anything except a bullet is an asteroid
        if (c.gameObject.tag != "Bullet")
        {

            // Move the ship to the centre of the screen
            transform.position = new Vector3(0, 0, 0);

            // Remove all velocity from the ship
            GetComponent<Rigidbody2D>().
                velocity = new Vector3(0, 0, 0);

            gameController.DecrementLives();
        }
    }

    void ShootBullet()
    {
        // Spawn a bullet
        Instantiate(bullet,
            new Vector3(transform.position.x, transform.position.y, 0),
            transform.rotation);
    }
}

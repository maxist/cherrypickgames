﻿using UnityEngine;
using System.Collections;

public class Pause : MonoBehaviour {

    public static Pause instance;
    public bool isPaused;								//Boolean to check if the game is paused or not
	private ShowPanels showPanels;                      //Reference to the ShowPanels script used to hide and show UI panels
    private bool tmpIsShowCrosshair;
    private StartOptions startScript;					//Reference to the StartButton script
    private ShopScript ShopScript;
    GameObject Player;
    GameController gameController;

    //Awake is called before Start()
    void Awake()
    {
        //If we don't currently have a game control...
        if (instance == null)
            //...set this one to be it...
            instance = this;
        //...otherwise...
        else if (instance != this)
            //...destroy this one because it is a duplicate.
            Destroy(gameObject);
        //Get a component reference to ShowPanels attached to this object, store in showPanels variable
        showPanels = GetComponent<ShowPanels> ();
		//Get a component reference to StartButton attached to this object, store in startScript variable
		startScript = GetComponent<StartOptions> ();
	}

	// Update is called once per frame
	void Update () {

		//Check if the Cancel button in Input Manager is down this frame (default is Escape key) and that game is not paused, and that we're not in main menu
		if (Input.GetButtonDown ("Cancel") && !isPaused && !startScript.inMainMenu) 
		{
            Debug.Log("pausing");
			//Call the DoPause function to pause the game
			DoPause();
		} 
		//If the button is pressed and the game is paused and not in main menu
		else if (Input.GetButtonDown ("Cancel") && isPaused && !startScript.inMainMenu) 
		{
			//Call the UnPause function to unpause the game
			UnPause ();
		}
	
	}

    public void DoPauseShop()
    {
        //Set isPaused to true
        isPaused = true;
        //Set time.timescale to 0, this will cause animations and physics to stop updating
        Time.timeScale = 0;

        Player = GameObject.FindGameObjectWithTag("Player");
        var weapon = Player.GetComponentInChildren<Weapon>();
        tmpIsShowCrosshair = weapon.showCrosshair;
        weapon.showCrosshair = false;
        weapon.enabled = false;
        // Player.GetComponent<FirstPersonCharacter>(). = false;
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        Player.GetComponent<FirstPersonCharacter>().advanced.CanMove = false;

        //call the ShowPausePanel function of the ShowPanels script
        showPanels.ShowShopPanel();
    }

    public void UnPauseShop()
    {
        //Set isPaused to false
        isPaused = false;
        //Set time.timescale to 1, this will cause animations and physics to continue updating at regular speed
        Time.timeScale = 1;

        gameController = GameController.instance;
        Player = GameObject.FindGameObjectWithTag("Player");
        if (Player != null)
        {
            var weaopn = Player.GetComponentInChildren<Weapon>();
            weaopn.showCrosshair = tmpIsShowCrosshair;
            weaopn.enabled = true;
            Player.GetComponent<FirstPersonCharacter>().lockCursor = true;
            Player.GetComponent<FirstPersonCharacter>().advanced.CanMove = true;
        }
        if (gameController != null)
            gameController.SetMouseSens(StatsGame.MouseSens);

        //call the HidePausePanel function of the ShowPanels script
        showPanels.HideShopPanel();

        gameController.NextLvl();
    }


    public void DoPause()
	{
		//Set isPaused to true
		isPaused = true;
		//Set time.timescale to 0, this will cause animations and physics to stop updating
		Time.timeScale = 0;

        Player = GameObject.FindGameObjectWithTag("Player");
        var weapon = Player.GetComponentInChildren<Weapon>();
        tmpIsShowCrosshair = weapon.showCrosshair;
        weapon.showCrosshair = false;
        weapon.enabled = false;
       // Player.GetComponent<FirstPersonCharacter>(). = false;
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.Confined;
        Player.GetComponent<FirstPersonCharacter>().advanced.CanMove = false;

        //call the ShowPausePanel function of the ShowPanels script
        showPanels.ShowPausePanel ();
	}


	public void UnPause()
	{
		//Set isPaused to false
		isPaused = false;
		//Set time.timescale to 1, this will cause animations and physics to continue updating at regular speed
		Time.timeScale = 1;

        gameController = GameController.instance;
        Player = GameObject.FindGameObjectWithTag("Player");
        if (Player != null)
        {
            var weaopn = Player.GetComponentInChildren<Weapon>();
            weaopn.showCrosshair = tmpIsShowCrosshair;
            weaopn.enabled = true;
            Player.GetComponent<FirstPersonCharacter>().lockCursor = true;
            Player.GetComponent<FirstPersonCharacter>().advanced.CanMove = true;
        }
        if(gameController != null)
        gameController.SetMouseSens(StatsGame.MouseSens);

        //call the HidePausePanel function of the ShowPanels script
        showPanels.HidePausePanel ();
	}


}

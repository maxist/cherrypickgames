WRP iOS Utilities
for Unity iOS Projects

by weRplay
werplay.com


Features:
---------

-	Show native iOS Dialog Box ( UIAlertView ) in your Unity application.
-	Supported dialog box styles are:
		1.	Normal Dialog Box
		2.	Dialog Box with a Text Field
		3.	Dialog Box with a Secure Text Field ( where the characters are hidden like password fields )
		4.	Dialog Box with a Login and Password Field


////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////


Usage:
------

1.	Import the package contents into your Unity project.

2.	Drag and Drop the WRPiOSUtilities prefab from the Project pane ( Assets>Plugins>WRPiOSUtilities>Prefab>WRPiOSUtilities ) to the Hierarchy pane in Untiy.

4.	Call the function of your choice as follows:

		WRPiOSUtilities.ShowDialogWithFiveButtons( "The Title", "The Message", "Cancel Button", "Second Title", "Third Title", null, null, 22 );

All available functions are listed at the end of this file, with a description of their purpose and usage.
We also recommend you take a look at the Test Scene ( specially the GUIManager.cs & SampleListenerWRPiOSUtilities.cs scripts at Assets>Plugins>WRPiOSUtilities>TestScene>Scripts ).


////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////


List of Functions:
------------------

Following arguments are used in the functions listed below:

string dialogTitle:		Sets the Title of the Dialog Box.
string dialogText:		Sets the message shown in the dialog box.

string cancelButtonTitle:		Cancel button is similar to other buttons in all respects except one. It has a slightly different look when the dialog box is shown on screen. Apart from that, it is the same as any other button. Its title can also be customised like others.

string secondButtonTitle, string thirdButtonTitle, string fourthButtonTitle, string fifthButtonTitle:
These are all titles to show on the buttons.

int idTag:		This is an identification number that you assign to the Dialog Box. This number is used to identify the dialog box when user hits a button and the listener function is called.

--------------------------------

Normal Dialog Boxes:

public static void ShowDialogWithCancelButton( string dialogTitle, string dialogText, string cancelButtonTitle, int idTag )
	-	Shows a simple dialog box with a single button.
	
public static void ShowDialogWithTwoButtons( string dialogTitle, string dialogText, string cancelButtonTitle, string secondButtonTitle, int idTag )
	-	Shows a simple dialog box with two buttons.

public static void ShowDialogWithThreeButtons( string dialogTitle, string dialogText, string cancelButtonTitle, string secondButtonTitle, string thirdButtonTitle, int idTag )
	-	Shows a simple dialog box with three buttons.
	
public static void ShowDialogWithFourButtons( string dialogTitle, string dialogText, string cancelButtonTitle, string secondButtonTitle, string thirdButtonTitle, string fourthButtonTitle, int idTag )
	-	Shows a simple dialog box with four buttons.

public static void ShowDialogWithFiveButtons( string dialogTitle, string dialogText, string cancelButtonTitle, string secondButtonTitle, string thirdButtonTitle, string fourthButtonTitle, string fifthButtonTitle, int idTag )
	-	Shows a simple dialog box with five buttons.

--------------------------------

public static void ShowNormalTextFieldDialogWithTwoButtons( string dialogTitle, string dialogText, string cancelButtonTitle, string secondButtonTitle, int idTag )
	-	Shows a dialog box with two buttons and a normal text field.
	-	To show only one button instead of two, simply pass "null" ( without the inverted commas ) as the fourth argument, in place of secondButtonTitle.
		e.g: WRPiOSUtilities.ShowNormalTextFieldDialogWithTwoButtons( "Dialog Title", "Custom Dialog Text", "OK", null, 1003;

--------------------------------
	
public static void ShowSecureTextFieldDialogWithTwoButtons( string dialogTitle, string dialogText, string cancelButtonTitle, string secondButtonTitle, int idTag )
	-	Shows a dialog box with two buttons and a secure text field. The secure text field hides its contents on the device screen.
	-	To show only one button instead of two, simply pass "null" ( without the inverted commas ) as the fourth argument, in place of secondButtonTitle.
		e.g: WRPiOSUtilities.ShowSecureTextFieldDialogWithTwoButtons( "Dialog Title", "Custom Dialog Text", "Cancel", null, 1005;

--------------------------------
	
public static void ShowLoginPasswordDialogWithTwoButtons( string dialogTitle, string dialogText, string cancelButtonTitle, string secondButtonTitle, int idTag )
	-	Shows a dialog box with two buttons, a login field and a password field.
	-	To show only one button instead of two, simply pass "null" ( without the inverted commas ) as the fourth argument, in place of secondButtonTitle.
		e.g: WRPiOSUtilities.ShowLoginPasswordDialogWithTwoButtons( "Dialog Title", "Custom Dialog Text", "Log In", null, 1007;

--------------------------------



Event Listeners:
----------------

One of the following 3 events is fired when a user taps a button on a dialog Box, depending on the type of the dialog box:
	

public static event Action < int, int, string > DialogButtonClickedEvent
	-	Fired for button tap in a Normal Dialog Box.
	-	First argument is the ID Tag of the dialog box.
	-	Second argument is the Index of the clicked button.
	-	Thrid argument is the Title of the cliked button.

public static event Action < int, int, string, string > TextFieldDialogButtonClickedEvent
	-	Fired for a button tap in a Dialog Box with one text field ( normal or secure text field ).
	-	First argument is the ID Tag of the dialog box.
	-	Second argument is the Index of the clicked button.
	-	Thrid argument is the Title of the cliked button.
	-	Fourth argument is the string enterd in the text field.

public static event Action < int, int, string, string[] > LoginPasswordDialogButtonClickedEvent
	-	Fired for a button tap in a Dialog Box with a login field and a password field.
	-	First argument is the ID Tag of the dialog box.
	-	Second argument is the Index of the clicked button.
	-	Thrid argument is the Title of the cliked button.
	-	Fourth argument is an array that holds the login ( at index 0 ) and password ( at index 1 ) field strings.

To understand the usage of these events, see the SampleListenerWRPiOSUtilities.cs scripts at Assets>Plugins>WRPiOSUtilities>TestScene>Scripts.


For more Unity plugins and tools, please visit http://www.werplay.com/unity-plugins.html
For further queries or information, please email us at unitysupport@werplay.com
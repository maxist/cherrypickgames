using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class GUIManager : MonoBehaviour
{
	float buttonHeight = Screen.height / 5;
	float buttonWidth = ( Screen.width / 2 ) - 20;

	
	void Start()
	{
		
	}

	void Update() 
	{
		buttonHeight = Screen.height / 5;
		buttonWidth =  ( Screen.width / 2 ) - 20;
	}
	
	void OnGUI ()
	{
		GUI.color = Color.black;
		float buttonPositionX = 10;
		float buttonPositionY = 10;
		
		GUI.color = Color.white;
		if( GUI.Button( new Rect( buttonPositionX, buttonPositionY, buttonWidth, buttonHeight ), "5 Button Dialog" ) )
		{
			WRPiOSUtilities.ShowDialogWithFiveButtons( "The Title", "The Message", "Cancel Title", "Second Title", "Third Title", "Fourth Title", "Fifth Title", 11 );
		}
		
		buttonPositionY += buttonHeight + 10;
		if( GUI.Button( new Rect( buttonPositionX, buttonPositionY, buttonWidth, buttonHeight ), "3 Button Dialog" ) )
		{
			WRPiOSUtilities.ShowDialogWithThreeButtons( "The Title", "The Message", "Cancel Title", "Second Title", "Third Title",  22 );
		}
		
		buttonPositionY += buttonHeight + 10;
		if( GUI.Button( new Rect( buttonPositionX, buttonPositionY, buttonWidth, buttonHeight ), "2 Button Dialog" ) )
		{
			WRPiOSUtilities.ShowDialogWithTwoButtons( "The Title", "The Message", "Cancel Title", "Second Title",  22 );
		}

		buttonPositionX += buttonWidth + 10;
		buttonPositionY = 10;
		if( GUI.Button( new Rect( buttonPositionX, buttonPositionY, buttonWidth, buttonHeight ), "2 Button Login Password Dialog" ) )
		{
			WRPiOSUtilities.ShowLoginPasswordDialogWithTwoButtons( "The Title", "Here goes the message.", "Cancel Title", "Second Title", 33 );
		}
		
		buttonPositionY += buttonHeight + 10;
		if( GUI.Button( new Rect( buttonPositionX, buttonPositionY, buttonWidth, buttonHeight ), "2 Button Secure Text Field Dialog" ) )
		{
			WRPiOSUtilities.ShowSecureTextFieldDialogWithTwoButtons( "The Title", "Here goes the message.", "Cancel Title", "Second Title", 44 );
		}
		
		buttonPositionY += buttonHeight + 10;
		if( GUI.Button( new Rect( buttonPositionX, buttonPositionY, buttonWidth, buttonHeight ), "1 Button Normal Text Field Dialog" ) )
		{
			WRPiOSUtilities.ShowNormalTextFieldDialogWithTwoButtons( "The Title", "Here goes the message.", null, "Sample Button", 55 );
		}
	}
}
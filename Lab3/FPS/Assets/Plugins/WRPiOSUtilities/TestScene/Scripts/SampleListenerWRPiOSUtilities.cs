using UnityEngine;
using System.Collections;

public class SampleListenerWRPiOSUtilities : MonoBehaviour
{

	void Start()
	{
		//WRPiOSUtilities.DialogButtonClickedEvent += OnDialogButtonClicked;
		//WRPiOSUtilities.TextFieldDialogButtonClickedEvent += OnTextFieldDialogButtonClicked;
		//WRPiOSUtilities.LoginPasswordDialogButtonClickedEvent += OnLoginPasswordDialogButtonClicked;
	}

	void OnDialogButtonClicked( int idTag, int buttonIndex, string buttonTitle )
	{
		Debug.Log( "Dialog Button Clicked. ID: " + idTag + ", Button Index: " + buttonIndex + ", Button Title: " + buttonTitle );
	}

	void OnTextFieldDialogButtonClicked( int idTag, int buttonIndex, string buttonTitle, string textFieldString )
	{
		Debug.Log( "Text Field Dialog Button Clicked. ID: " + idTag + ", Button Index: " + buttonIndex + ", Button Title: " + buttonTitle + ", Text: " + textFieldString );
	}

	void OnLoginPasswordDialogButtonClicked( int idTag, int buttonIndex, string buttonTitle, string[] textStringsArray )
	{
		string loginFieldString = textStringsArray[ 0 ];
		string passwordFieldString = textStringsArray[ 1 ];
		Debug.Log( "Login Password Dialog Button Clicked. ID: " + idTag + ", Button Index: " + buttonIndex + ", Button Title: " + buttonTitle + ", Login: " + loginFieldString + ", Password: " + passwordFieldString );
	}
}
                 
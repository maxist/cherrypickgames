using UnityEngine;
using System.Collections;
using System;
using System.Runtime.InteropServices;


public class WRPiOSUtilities  : MonoBehaviour
{
	[ DllImport ( "__Internal" ) ]
	private static extern void _showLoginPasswordDialogWithTitleAndMessageAndCancelTitleAndSecondTitleAndThirdTitle( string boxTitle, string boxMessage,  string cancelTitle, string secondTitle, string thirdTitle, int idTag );
	
	[ DllImport ( "__Internal" ) ]
	private static extern void _showSecureTextFieldDialogWithTitleAndMessageAndCancelTitleAndSecondTitleAndThirdTitle( string boxTitle, string boxMessage, string cancelTitle, string secondTitle, string thirdTitle, int idTag );
	
	[ DllImport ( "__Internal" ) ]
	private static extern void _showNormalTextFieldDialogWithTitleAndMessageAndCancelTitleAndSecondTitleAndThirdTitle( string boxTitle, string boxMessage, string cancelTitle, string secondTitle, string thirdTitle, int idTag );
	
	[ DllImport ( "__Internal" ) ]
	private static extern void _showDialogWithTitleAndMessageAndCancelTitleAndSecondTitleAndThirdTitleAndFourthTitleAndFifthTitle( string boxTitle, string boxMessage, string cancelTitle, string secondTitle, string thirdTitle, string fourthTitle, string fifthTitle, int idTag );

	[ DllImport ( "__Internal" ) ]
	private static extern void _showDialogWithTitleAndMessageAndCancelTitleAndOtherButtonTitlesArray( string boxTitle, string boxMessage, string cancelTitle, string[] otherButtonsTitlesArray, int idTag );
	
	[ DllImport ( "__Internal" ) ]
	private static extern void _setObjectName( string givenName );
	
	
	
	public static WRPiOSUtilities instance;
	private string gameObjectName = "WRPiOSUtilities";
	
	
	void Start()
	{
		instance = this;
		DontDestroyOnLoad( gameObject );
		if( gameObject != null && gameObject.name != null )
		{
			gameObjectName = gameObject.name;
			if( !Application.isEditor )
			{
				_setObjectName( gameObjectName );
			}
		}
		else
		{
			Debug.LogWarning( "GameObject Not Found" );
		}
	}
	
	//Shows a simple dialog box with a single button
	//Set the Title of dialog box using the argument string dialogTitle
	//Set the message shown in the dialog box using the argument string dialogText
	//Customise the button title the argument string cancelButtonTitle
	//Set an identification number for the Dialog Box using the argument int idTag. This number is used to identify the dialog box when user hits a button and the listener function is called.
	public static void ShowDialogWithCancelButton( string dialogTitle, string dialogText, string cancelButtonTitle, int idTag )
	{
		if ( !Application.isEditor )
		{
			_showDialogWithTitleAndMessageAndCancelTitleAndSecondTitleAndThirdTitleAndFourthTitleAndFifthTitle( dialogTitle, dialogText, cancelButtonTitle, null, null, null, null, idTag );
		}
	}
	
	//Shows a simple dialog box with two buttons.
	//Set the Title of dialog box using the argument string dialogTitle
	//Set the message shown in the dialog box using the argument string dialogText
	//Customise the button titles using the arguments string cancelButtonTitle, string secondButtonTitle
	//Set an identification number for the Dialog Box using the argument int idTag. This number is used to identify the dialog box when user hits a button and the listener function is called.
	public static void ShowDialogWithTwoButtons( string dialogTitle, string dialogText, string cancelButtonTitle, string secondButtonTitle, int idTag )
	{
		if ( !Application.isEditor )
		{
			_showDialogWithTitleAndMessageAndCancelTitleAndSecondTitleAndThirdTitleAndFourthTitleAndFifthTitle( dialogTitle, dialogText, cancelButtonTitle, secondButtonTitle, null, null, null, idTag );
		}
	}

	//Shows a simple dialog box with three buttons.
	//Set the Title of dialog box using the argument string dialogTitle
	//Set the message shown in the dialog box using the argument string dialogText
	//Customise the button titles using the arguments string cancelButtonTitle, string secondButtonTitle, string thirdButtonTitle
	//Set an identification number for the Dialog Box using the argument int idTag. This number is used to identify the dialog box when user hits a button and the listener function is called.
	public static void ShowDialogWithThreeButtons( string dialogTitle, string dialogText, string cancelButtonTitle, string secondButtonTitle, string thirdButtonTitle, int idTag )
	{
		if ( !Application.isEditor )
		{
			_showDialogWithTitleAndMessageAndCancelTitleAndSecondTitleAndThirdTitleAndFourthTitleAndFifthTitle( dialogTitle, dialogText, cancelButtonTitle, secondButtonTitle, thirdButtonTitle, null, null, idTag );
		}
	}

	//Shows a simple dialog box with four buttons.
	//Set the Title of dialog box using the argument string dialogTitle
	//Set the message shown in the dialog box using the argument string dialogText
	//Customise the button titles using the arguments string cancelButtonTitle, string secondButtonTitle, string thirdButtonTitle, string fourthButtonTitle
	//Set an identification number for the Dialog Box using the argument int idTag. This number is used to identify the dialog box when user hits a button and the listener function is called.
	public static void ShowDialogWithFourButtons( string dialogTitle, string dialogText, string cancelButtonTitle, string secondButtonTitle, string thirdButtonTitle, string fourthButtonTitle, int idTag )
	{
		if ( !Application.isEditor )
		{
			_showDialogWithTitleAndMessageAndCancelTitleAndSecondTitleAndThirdTitleAndFourthTitleAndFifthTitle( dialogTitle, dialogText, cancelButtonTitle, secondButtonTitle, thirdButtonTitle, fourthButtonTitle, null, idTag );
		}
	}
	
	//Shows a simple dialog box with five buttons.
	//Set the Title of dialog box using the argument string dialogTitle
	//Set the message shown in the dialog box using the argument string dialogText
	//Customise the button titles using the arguments string cancelButtonTitle, string secondButtonTitle, string thirdButtonTitle, string fourthButtonTitle, string fifthButtonTitle
	//Set an identification number for the Dialog Box using the argument int idTag. This number is used to identify the dialog box when user hits a button and the listener function is called.
	public static void ShowDialogWithFiveButtons( string dialogTitle, string dialogText, string cancelButtonTitle, string secondButtonTitle, string thirdButtonTitle, string fourthButtonTitle, string fifthButtonTitle, int idTag )
	{
		if ( !Application.isEditor )
		{
			_showDialogWithTitleAndMessageAndCancelTitleAndSecondTitleAndThirdTitleAndFourthTitleAndFifthTitle( dialogTitle, dialogText, cancelButtonTitle, secondButtonTitle, thirdButtonTitle, fourthButtonTitle, fifthButtonTitle, idTag );
		}
	}
	
	//Shows a dialog box with two buttons and a normal text field.
	//Set the Title of dialog box using the argument string dialogTitle
	//Set the message shown in the dialog box using the argument string dialogText
	//Customise the button titles using the arguments string cancelButtonTitle, string secondButtonTitle
	//Set an identification number for the Dialog Box using the argument int idTag. This number is used to identify the dialog box when user hits a button and the listener function is called.
	//To show only one button instead of two, simply pass "null" ( without the inverted commas ) as the fourth argument, in place of secondButtonTitle.
	//	e.g: WRPiOSUtilities.ShowNormalTextFieldDialogWithTwoButtons( "Dialog Title", "Custom Dialog Text", "OK", null, 1003;
	public static void ShowNormalTextFieldDialogWithTwoButtons( string dialogTitle, string dialogText, string cancelButtonTitle, string secondButtonTitle, int idTag )
	{
		if ( !Application.isEditor )
		{
			_showNormalTextFieldDialogWithTitleAndMessageAndCancelTitleAndSecondTitleAndThirdTitle( dialogTitle, dialogText, cancelButtonTitle, secondButtonTitle, null, idTag );
		}
	}
	
	//Shows a dialog box with two buttons and a secure text field. The secure text field hides its contents on the device screen.
	//Set the Title of dialog box using the argument string dialogTitle
	//Set the message shown in the dialog box using the argument string dialogText
	//Customise the button titles using the arguments string cancelButtonTitle, string secondButtonTitle
	//Set an identification number for the Dialog Box using the argument int idTag. This number is used to identify the dialog box when user hits a button and the listener function is called.
	//To show only one button instead of two, simply pass "null" ( without the inverted commas ) as the fourth argument, in place of secondButtonTitle.
	//	e.g: WRPiOSUtilities.ShowSecureTextFieldDialogWithTwoButtons( "Dialog Title", "Custom Dialog Text", "Cancel", null, 1005;
	public static void ShowSecureTextFieldDialogWithTwoButtons( string dialogTitle, string dialogText, string cancelButtonTitle, string secondButtonTitle, int idTag )
	{
		if ( !Application.isEditor )
		{
			_showSecureTextFieldDialogWithTitleAndMessageAndCancelTitleAndSecondTitleAndThirdTitle( dialogTitle, dialogText, cancelButtonTitle, secondButtonTitle, null, idTag );
		}
	}
	
	//Shows a dialog box with two buttons, a login field and a password field.
	//Set the Title of dialog box using the argument string dialogTitle
	//Set the message shown in the dialog box using the argument string dialogText
	//Customise the button titles using the arguments string cancelButtonTitle, string secondButtonTitle
	//Set an identification number for the Dialog Box using the argument int idTag. This number is used to identify the dialog box when user hits a button and the listener function is called.
	//To show only one button instead of two, simply pass "null" ( without the inverted commas ) as the fourth argument, in place of secondButtonTitle.
	//	e.g: WRPiOSUtilities.ShowLoginPasswordDialogWithTwoButtons( "Dialog Title", "Custom Dialog Text", "Log In", null, 1007;
	public static void ShowLoginPasswordDialogWithTwoButtons( string dialogTitle, string dialogText, string cancelButtonTitle, string secondButtonTitle, int idTag )
	{
		if ( !Application.isEditor )
		{
			_showLoginPasswordDialogWithTitleAndMessageAndCancelTitleAndSecondTitleAndThirdTitle( dialogTitle, dialogText, cancelButtonTitle, secondButtonTitle, null, idTag );
		}
	}
	



	#if UNITY_IPHONE


	public static event Action < int, int, string > DialogButtonClickedEvent;		//Fired for Normal Dialog Box
	public static event Action < int, int, string, string > TextFieldDialogButtonClickedEvent;		//Fired for a Dialog Box with one text field
	public static event Action < int, int, string, string[] > LoginPasswordDialogButtonClickedEvent;		//Fired for a Dialog Box with login and password fields


	public void	alertButtonClicked( string buttonHitData )
	{
		Debug.Log( "alertButtonClicked" );
		
		//Splits the string to retrive useful information
		string[] stringsArray = buttonHitData.Split('^');
		
		int idTag = int.Parse( stringsArray[ 0 ] );
		int buttonIndex = int.Parse( stringsArray[ 1 ] );
		string buttonTitle = stringsArray[ 2 ];

		int numberOfTextFields = int.Parse( stringsArray[ 3 ] );
		string firstTextFieldContent = "";
		string secondTextFieldContent = "";
		
		if( numberOfTextFields == 1 )		//Normal OR Secure Input Text Field
		{
			firstTextFieldContent = ( stringsArray[ 4 ] != null ) ? ( stringsArray[ 4 ] ) : "";

			if( TextFieldDialogButtonClickedEvent != null )
			{
				TextFieldDialogButtonClickedEvent( idTag, buttonIndex, buttonTitle, firstTextFieldContent );
			}
		}
		else if( numberOfTextFields == 2 )		//Login / Password Fields
		{
			firstTextFieldContent = ( stringsArray[ 4 ] != null ) ? ( stringsArray[ 4 ] ) : "";		//Login Field
			secondTextFieldContent = ( stringsArray[ 5 ] != null ) ? ( stringsArray[ 5 ] ) : "";		//Password
			
			string[] textFieldStrings = { firstTextFieldContent, secondTextFieldContent };

			if( LoginPasswordDialogButtonClickedEvent != null )
			{
				LoginPasswordDialogButtonClickedEvent( idTag, buttonIndex, buttonTitle, textFieldStrings );
			}
		}
		else		//Normal Alert Dialog without text fields
		{
			if( DialogButtonClickedEvent != null )
			{
				DialogButtonClickedEvent( idTag, buttonIndex, buttonTitle );
			}
		}
	}

	#endif
}
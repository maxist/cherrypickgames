﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{

    public static GameController instance;

    public Text CashText;
    public Text HPText;
    public Text MainText;
    public Scrollbar HP;
    public AudioClip windSound;
    public AudioMixerGroup musicLvl;

    private bool wasRestart = false;
    private int indexOfEnabledSpawners;
    private int amountOfSpawnZombies;
    private int maxAmountOfZombiesPerLvl;
    private int indexOfDisabledSpawnersZombie;
    private int indexOfRespawnPlayer;
    private int indexOfActiveTerrain;
    private float nextActionTime = 0.0f;
    private float ratio;
    GameObject[] terrains;
    GameObject[] spawners;
    GameObject[] zombies;
    GameObject[] placeWhereMusicVolumeUp;
    GameObject Player;
    GameObject MainDoor;
    AudioSource audioSource;
    GameObject[] respawnsPlayer;
    Pause pause;
    List<int> LvlsToAddSpawner = new List<int>();
    System.Random random;
    ShopScript shopScript;

    public int AmountOfSpawnedZombies
    {
        get { return amountOfSpawnZombies; }
        set
        {
            amountOfSpawnZombies = value;
            CheckIfIsMaxZombies();
        }
    }
    public bool CharacterInsideHouse { get; set; }
    public float Lvl { get; set; }
    public float DifficultyModificator { get; set; }
    public float MaxLvl { get; set; }

    private void Awake()
    {
        //If we don't currently have a game control...
        if (instance == null)
            //...set this one to be it...
            instance = this;
        //...otherwise...
        else if (instance != this)
            //...destroy this one because it is a duplicate.
            Destroy(gameObject);

        random = new System.Random();
        audioSource = GetComponent<AudioSource>();
        if (audioSource == null)
            audioSource = gameObject.AddComponent<AudioSource>();
        audioSource.outputAudioMixerGroup = musicLvl;
        
        pause = Pause.instance;

        spawners = GameObject.FindGameObjectsWithTag("Spawner").OrderBy(x => x.name).ToArray();
        foreach (var spawner in spawners)
        {
            spawner.GetComponent<Spawner>().isSpawnEnemy = false;
        }

        terrains = GameObject.FindGameObjectsWithTag("Terrain").OrderBy(x => x.name).ToArray();
        respawnsPlayer = GameObject.FindGameObjectsWithTag("Respawn").OrderBy(x => x.name).ToArray();
        placeWhereMusicVolumeUp = GameObject.FindGameObjectsWithTag("VolumeUpPlace").ToArray();
        foreach (var terrain in terrains)
        {
            terrain.SetActive(false);
        }
        terrains[0].SetActive(true);


        Player = GameObject.FindGameObjectWithTag("Player");
        MainDoor = GameObject.Find("MainDoor");

        SetDifficulty(StatsGame.difficultyLevel);
        SetMouseSens(StatsGame.MouseSens);

        audioSource.volume = 1 / Vector3.Distance(Player.transform.position, MainDoor.transform.position);
        Lvl = 1;
        MainText.text = "";
        HP.size = 1;
        indexOfDisabledSpawnersZombie = 0;
        indexOfRespawnPlayer = 0;
        indexOfActiveTerrain = 0;
        MaxLvl = terrains.Length * 5;
        ratio = MaxLvl / spawners.Length;
        CharacterInsideHouse = true;
        CreateSpawnLvl();
        CheckIfAddAnotherSpawner();
        SetSpawnersToSpawn();
    }

    private void Start()
    {
        audioSource.playOnAwake = true;
        audioSource.loop = true;
        audioSource.clip = windSound;
        audioSource.Play();
        StatsGame.Cash = 5000;
        HPText.text = Player.GetComponent<Health>().maxHealth + " / " + Player.GetComponent<Health>().maxHealth;
        CashText.text = "Cash: " + String.Format("{0:0.00}", StatsGame.Cash);

        Player.transform.position = respawnsPlayer[indexOfRespawnPlayer].transform.position;
    }


    // Update is called once per frame
    void Update()
    {
        CashText.text = "Cash: " + String.Format("{0:0.00}", StatsGame.Cash);

        if (wasRestart)
        {
            if (Time.time > nextActionTime)
            {
                wasRestart = false;
            }
        }
        else
        {
            if (CheckIfAllZombiesAreDead())
            {
                EndLvl();
            }
        }

        if (terrains[0].activeInHierarchy)
        {
            if (CharacterInsideHouse)
            {
                audioSource.volume = 1 / Vector3.Distance(Player.transform.position, MainDoor.transform.position);
                foreach (var item in placeWhereMusicVolumeUp)
                {
                    var tmpDist = Vector3.Distance(Player.transform.position, item.transform.position);
                    if (audioSource.volume < 1 / tmpDist)
                        audioSource.volume = 1 / tmpDist;
                }
            }
            else
                audioSource.volume = 1;
        }
        else
        {
            audioSource.Stop();
        }
    }

    void EndLvl()
    {
        if (Lvl == MaxLvl)
            FinishGame();
        else
            StartCoroutine(RestartLvl());
    }

    private IEnumerator RestartLvl()
    {
        wasRestart = true;
        MainText.text = "Good job. Stage " + Lvl + " is clear now.";
        AmountOfSpawnedZombies = 0;
        maxAmountOfZombiesPerLvl = random.Next(maxAmountOfZombiesPerLvl, maxAmountOfZombiesPerLvl * 2);
        nextActionTime = Time.time + 3f;
        yield return new WaitForSeconds(3f);
        pause.DoPauseShop();
    }

    public void NextLvl()
    {
        MainText.text = "";
        if (Lvl % 5 == 0)
            indexOfRespawnPlayer++;
        Player.transform.position = respawnsPlayer[indexOfRespawnPlayer].transform.position;
        CheckIfChangeTerrain();
        CheckIfAddAnotherSpawner();
        SetSpawnersToSpawn();
        Lvl++;
    }

    private void FinishGame()
    {
        MainText.text = "Congratulations. You survived all levels.";
        Player.GetComponent<FirstPersonCharacter>().advanced.CanMove = false;
    }

    void CheckIfChangeTerrain()
    {
        if (Lvl % 5 == 0)
        {
            indexOfActiveTerrain++;

            for (int i = 0; i < terrains.Length; i++)
            {
                if (indexOfActiveTerrain == i)
                    terrains[i].SetActive(true);
                else
                    terrains[i].SetActive(false);
            }
        }
    }

    private bool CheckIfAllZombiesAreDead()
    {
        if (AmountOfSpawnedZombies < maxAmountOfZombiesPerLvl) return false;

        zombies = GameObject.FindGameObjectsWithTag("Enemy");
        if (zombies == null || zombies.Length <= 0)
            return true;

        return false;
    }

    private void CheckIfIsMaxZombies()
    {
        if (AmountOfSpawnedZombies >= maxAmountOfZombiesPerLvl)
        {
            foreach (var item in spawners)
            {
                item.GetComponent<Spawner>().isSpawnEnemy = false;
            }
        }
    }

    private void SetSpawnersToSpawn()
    {
        for (int i = indexOfDisabledSpawnersZombie; i < indexOfEnabledSpawners; i++)
        {
            var freq = spawners[i].GetComponent<Spawner>().spawnFrequency;
            spawners[i].GetComponent<Spawner>().spawnFrequency = freq > 1 ? (freq - 0.5f) : freq;
            spawners[i].GetComponent<Spawner>().isSpawnEnemy = true;
        }
    }

    private void CheckIfAddAnotherSpawner()
    {
        if (LvlsToAddSpawner.Contains((int)Lvl))
        {
            indexOfEnabledSpawners++;
        }

        if (Lvl % 5 == 0)
            indexOfDisabledSpawnersZombie += 5;
    }

    private void CreateSpawnLvl()
    {
        LvlsToAddSpawner.Add(1);
        for (int i = 1; i < spawners.Length; i++)
        {
            LvlsToAddSpawner.Add((int)Math.Round(ratio * i) + 1);
        }
    }

    public void UpdatePlayerHPUI(float MaxHealth, float currentHealth)
    {
        HP.size = currentHealth / MaxHealth;
        HPText.text = currentHealth >= 0 ? (int)currentHealth + " / " + MaxHealth : 0 + " / " + MaxHealth;
    }


    public IEnumerator GameOver()
    {
        MainText.text = "Game over";
        Player.GetComponent<FirstPersonCharacter>().advanced.CanMove = false;
        yield return new WaitForSeconds(5f);
        SceneManager.LoadScene(0);
    }

    public void SetMouseSens(float sensLvl)
    {
        Player.GetComponent<MouseRotator>().rotationSpeed = sensLvl;
    }

    public void SetDifficulty(DifficultyLevel difficultyLevel)
    {
        if (difficultyLevel == DifficultyLevel.Easy)
        {
            DifficultyModificator = 1;
        }

        if (difficultyLevel == DifficultyLevel.Medium)
        {
            DifficultyModificator = 1.5f;
        }

        if (difficultyLevel == DifficultyLevel.Hard)
        {
            DifficultyModificator = 2f;
        }

        maxAmountOfZombiesPerLvl = 5;
    }

    public IEnumerator FlashWhenHit(Image damageImage)
    {
        StartCoroutine(Fade(0, 0.8f, 0.01f, damageImage));
        yield return new WaitForSeconds(0.5f);
        StartCoroutine(Fade(0.8f, 0f, 0.01f, damageImage));
    }

    public void SetTransparentWhenZombieDie(Image damageImage)
    {
        var tmpColor = damageImage.color;
        tmpColor.a = 0;
        damageImage.color = tmpColor; // ensure the fade is completely finished (because lerp doesn't always end on an exact value)
    }

    IEnumerator Fade(float start, float end, float length, Image currentObject)
    { //define Fade parmeters
        if (currentObject.color.a == start)
        {

            var tmpColor = currentObject.color;
            for (float i = 0.0f; i < 1.0f; i += Time.deltaTime * (1 / length))
            { //for the length of time

                tmpColor.a = Mathf.Lerp(start, end, i); //lerp the value of the transparency from the start value to the end value in equal increments
                currentObject.color = tmpColor;
                yield return new WaitForSeconds(0.01f);
            } //end for
            tmpColor.a = end;
            currentObject.color = tmpColor; // ensure the fade is completely finished (because lerp doesn't always end on an exact value)
        } //end if
    } //end Fade
}

public enum DifficultyLevel
{
    Easy,
    Medium,
    Hard
}

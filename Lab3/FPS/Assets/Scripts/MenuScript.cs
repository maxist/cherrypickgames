﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuScript : MonoBehaviour
{

    private GameController gameController;
    public Dropdown dropdown;

    private void Awake()
    {
        gameController = GameController.instance;
        if (dropdown != null)
        {
            dropdown.options.Clear();
            dropdown.options.Add(new Dropdown.OptionData() { text = "Ease" });
            dropdown.options.Add(new Dropdown.OptionData() { text = "Medium" });
            dropdown.options.Add(new Dropdown.OptionData() { text = "Hard" });
        }
    }

    public void SetDiff()
    {
        int selectedIndex = dropdown.value;
        switch (selectedIndex)
        {
            case 0:
                StatsGame.difficultyLevel = DifficultyLevel.Easy;
                break;
            case 1:
                StatsGame.difficultyLevel = DifficultyLevel.Medium;
                break;
            case 2:
                StatsGame.difficultyLevel = DifficultyLevel.Hard;
                break;
            default:
                StatsGame.difficultyLevel = DifficultyLevel.Medium;
                break;
        }
    }

    public void SetMouseSensitivity(float mSens)
    {
        StatsGame.MouseSens = mSens;
    }
}

﻿using System;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class ShopScript : MonoBehaviour
{
    public Text alertInfo;
    public Text CashText;
    public Pause pause;
    public WeaponSystem WeaponSystem;

    private decimal PrizeM4 = 3000;
    private decimal PrizeShotgun = 2500;
    private decimal costper1HP = 10;
    private GameObject Player;
    private GameController gameController;
    private decimal costper1HPMAX = 20;
    private int numberOfAddedHPToMaxHP = 100;

    private void Awake()
    {
        CashText.text = "Cash: " + String.Format("{0:0.00}", StatsGame.Cash);
        alertInfo.text = "";
    }

    // Use this for initialization
    void Start()
    {
        WeaponSystem = WeaponSystem.instance;
        pause = Pause.instance;
        Player = GameObject.FindGameObjectWithTag("Player");
        gameController = GameController.instance;
    }

    // Update is called once per frame
    void Update()
    {
        CashText.text = "Cash: " + String.Format("{0:0.00}", StatsGame.Cash);
    }

    public void GoNextLevel()
    {
        alertInfo.text = "";
        pause.UnPauseShop();
    }

    public void BuyM4()
    {
        if (StatsGame.Cash < PrizeM4)
        {
            alertInfo.text = "You cannot buy M4. You do not have enough money";
            return;
        }

        StatsGame.Cash -= PrizeM4;
        WeaponSystem.AllowedIndexWeapon.Add(WeaponsName.M4);
        alertInfo.text = "Congratz! M4 is now in your inventory! Great!";

    }

    public void BuyShotgun()
    {
        if (StatsGame.Cash < PrizeShotgun)
        {
            alertInfo.text = "Sorry. You cannot buy Shotgun. You do not have enough money";
            return;
        }

        StatsGame.Cash -= PrizeShotgun;
        WeaponSystem.AllowedIndexWeapon.Add(WeaponsName.Shotgun);
        alertInfo.text = "Congratz! Shotgun is now in your inventory! Great!";
    }

    public void HealUp()
    {
        float MaxHP = Player.GetComponent<Health>().maxHealth;
        float currnetHP = Player.GetComponent<Health>().currentHealth;
        float toHealUpHP = MaxHP - currnetHP;
        decimal costOfHeal = (decimal)toHealUpHP * costper1HP;

        if (StatsGame.Cash < costOfHeal)
        {
            int amountOfHP = (int)Math.Floor(StatsGame.Cash / costper1HP);
            StatsGame.Cash -= amountOfHP * costper1HP;
            Player.GetComponent<Health>().currentHealth += amountOfHP;
            alertInfo.text = "You heal up. +" + amountOfHP + "HP";
            gameController.UpdatePlayerHPUI(MaxHP, Player.GetComponent<Health>().currentHealth);
        }
        else
        {
            StatsGame.Cash -= costOfHeal;
            Player.GetComponent<Health>().currentHealth = MaxHP;
            alertInfo.text = "Your HP is full now.";
            gameController.UpdatePlayerHPUI(MaxHP, MaxHP);
        }
    }

    public void IncreaseHealth()
    {
        decimal costOf = (decimal)costper1HPMAX * numberOfAddedHPToMaxHP;

        if (StatsGame.Cash < costOf)
        {
            alertInfo.text = "Sorry. You cannot buy Shotgun. You do not have enough money";
            return;
        }

        StatsGame.Cash -= costOf;
        Player.GetComponent<Health>().currentHealth += numberOfAddedHPToMaxHP;
        Player.GetComponent<Health>().maxHealth += numberOfAddedHPToMaxHP;
        alertInfo.text = "Your MaxHP increase.";
        gameController.UpdatePlayerHPUI(Player.GetComponent<Health>().maxHealth, Player.GetComponent<Health>().currentHealth);
    }
}

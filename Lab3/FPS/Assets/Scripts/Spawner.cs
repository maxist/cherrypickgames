﻿/// <summary>
/// Spawner.cs
/// Author: MutantGopher
/// This is a sample spawning script used to spawn the red cubes in the demo scene.
/// </summary>

using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour
{
    public GameObject prefabToSpawn;                // The prefab that should be spawned
    public float spawnFrequency = 6.0f;             // The time (in seconds) between spawns
    public bool spawnOnStart = false;               // Whether or not one instance of the prefab should be spawned on Start()
    public bool move = true;                        // Move this spawn spot around
    public float moveAmount = 5.0f;                 // The amount to move
    public float turnAmount = 5.0f;					// The amount to turn
    public bool isSpawnEnemy = true;

    GameController gameController;
    private float spawnTimer = 0.0f;

    // Use this for initialization
    void Start()
    {
        gameController = GameController.instance;
        if (spawnOnStart)
        {
            Spawn();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!isSpawnEnemy) return;

        // Update the spawning timer
        spawnTimer += Time.deltaTime;

        // Spawn a prefab if the timer has reached spawnFrequency
        if (spawnTimer >= spawnFrequency)
        {
            // First reset the spawn timer to 0
            spawnTimer = 0.0f;
            Spawn();
        }

        if (move)
        {
            // Move and turn so that boxes don't keep spawning in the same spots
            transform.Translate(0, 0, moveAmount);
            transform.Rotate(0, turnAmount, 0);
        }
    }

    public void Spawn()
    {
        // First check to see if the prefab hasn't been set
        if (prefabToSpawn != null)
        {
            // Instantiate the prefab
            var height = Terrain.activeTerrain.SampleHeight(transform.position)
       + Terrain.activeTerrain.transform.position.y;
            var zombie = Instantiate(prefabToSpawn, new Vector3(transform.position.x, height, transform.position.z), Quaternion.identity);
            RandomizeTheStrengthOfZombies(zombie);
            gameController.AmountOfSpawnedZombies++;
        }
    }

    private void RandomizeTheStrengthOfZombies(GameObject zombie)
    {
        var maxRangeAttackSpeed = (0.5f * gameController.Lvl) > 3 ? (0.5f * gameController.Lvl) : 3;

        var maxHealth = Random.Range(5 * gameController.Lvl, 50 * gameController.Lvl);
        var attackSpeed = Random.Range((1f / 8f) * gameController.Lvl, maxRangeAttackSpeed);
        var walingSpeed = Random.Range((1f / 4f) * gameController.Lvl, 0.5f * gameController.Lvl);
        var damage = Random.Range(5 * gameController.Lvl, 10 * gameController.Lvl);

        var ratio_maxHealth = (maxHealth / (50 * gameController.Lvl));
        var ratio_attackSpeed = attackSpeed / maxRangeAttackSpeed;
        var ratio_walingSpeed = walingSpeed / (0.5f * gameController.Lvl);
        var radio_damage = damage / (10 * gameController.Lvl);

        zombie.GetComponent<Health>().currentHealth = maxHealth * gameController.DifficultyModificator;
        zombie.GetComponent<Health>().maxHealth = maxHealth * gameController.DifficultyModificator;
        zombie.GetComponent<Zombie>().attackSpeed = attackSpeed * gameController.DifficultyModificator;
        zombie.GetComponent<Zombie>().walkingSpeed = (walingSpeed * gameController.DifficultyModificator) > 1 ? (walingSpeed * gameController.DifficultyModificator) : 1;
        zombie.GetComponent<Zombie>().damage = damage * gameController.DifficultyModificator;

        if (zombie.GetComponent<Zombie>().walkingSpeed >= GameObject.FindGameObjectWithTag("Player").GetComponent<FirstPersonCharacter>().runSpeed)
            zombie.GetComponent<Zombie>().walkingSpeed = GameObject.FindGameObjectWithTag("Player").GetComponent<FirstPersonCharacter>().runSpeed - 1;
        if (zombie.GetComponent<Zombie>().attackSpeed > 3)
            zombie.GetComponent<Zombie>().attackSpeed = 3;

        zombie.transform.localScale *= ((maxHealth / (50 * gameController.Lvl)) + 1);
        zombie.GetComponent<Zombie>().WorthOfZombie = (decimal)(gameController.Lvl * 10 * (radio_damage + ratio_attackSpeed + ratio_maxHealth + ratio_walingSpeed) / 4);
    }
}


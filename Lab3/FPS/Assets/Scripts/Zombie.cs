﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class Zombie : MonoBehaviour
{

    public float walkingSpeed = 1f;
    public float attackSpeed = 1f;
    public float damage = 10f;
    public float range = 100f;
    public AudioClip walkingSound;
    public AudioClip attackSound;
    public AudioClip fallbackSound;
    public AudioClip bloodSound;
    public AudioClip growlSound;
    public AudioClip idleSound;
    public AudioMixerGroup audioMixerFX;

    GameController gameController;
    GameObject Player;
    Animator anim;
    Image damageImage;
    Health Health;
    AudioSource audioSource;
    System.Random random;
    int numberWhenGrowlSound;
    int numberOfWalkingSound;
    int attackHash = Animator.StringToHash("CanAttack");
    int speedHash = Animator.StringToHash("Speed");
    int isDeadHash = Animator.StringToHash("IsDead");
    int isPlayerDeadHash = Animator.StringToHash("PlayerDead");
    int walk_in_placeStateHash = Animator.StringToHash("Base Layer.walk_in_place");
    int walkStateHash = Animator.StringToHash("Base Layer.walk");
    int attackStateHash = Animator.StringToHash("Base Layer.attack");
    int idleStateHash = Animator.StringToHash("Base Layer.idle");
    private bool isDead = false;
    private float deltaTimeHitByZombie;
    private float nextActionTime = 0.0f;

    public decimal WorthOfZombie { get; set; }

    private void Awake()
    {
        Health = GetComponent<Health>();
    }

    // Use this for initialization
    void Start()
    {
        gameController = GameController.instance;
        Player = GameObject.FindGameObjectWithTag("Player");
        anim = GetComponent<Animator>();

        GameObject imageObject = GameObject.FindGameObjectWithTag("BloodOnScreen");
        if (imageObject != null)
        {
            damageImage = imageObject.GetComponent<Image>();
        }

        deltaTimeHitByZombie = 1 / (2 * attackSpeed);

        audioSource = GetComponent<AudioSource>();
        if (audioSource == null)
        {
            // we automatically add an audiosource, if one has not been manually added.
            // (if you want to control the rolloff or other audio settings, add an audiosource manually)
            audioSource = gameObject.AddComponent<AudioSource>();
        }
        audioSource.outputAudioMixerGroup = audioMixerFX;

        random = new System.Random();
        numberWhenGrowlSound = random.Next(1, 10);
    }

    // Update is called once per frame
    void Update()
    {
        if (isDead) return;

        if (Player.GetComponent<Health>().currentHealth <= 0)
        {
            anim.SetBool(attackHash, false);
            anim.SetFloat(speedHash, 0);
            anim.SetBool(isPlayerDeadHash, true);
            anim.speed = 1;
            anim.Play(idleStateHash);

            audioSource.clip = idleSound;
            audioSource.Play();
            return;
        }

        Vector3 hostPos = gameObject.transform.position;
        Vector3 targetPos = Player.transform.position;

        if (Vector3.Distance(hostPos, targetPos) < range)
        {
            float step = walkingSpeed * Time.deltaTime;

            var distance = Vector3.Distance(this.transform.position, Player.transform.position);

            audioSource.volume = 1 / distance;

            if (distance > 1.6)
            {
                anim.SetBool(attackHash, false);
                anim.SetFloat(speedHash, walkingSpeed);
                anim.Play(walkStateHash);
                anim.speed = walkingSpeed;


                if (!audioSource.isPlaying)
                {
                    if (numberOfWalkingSound >= numberWhenGrowlSound)
                    {
                        audioSource.PlayOneShot(growlSound);
                        numberOfWalkingSound = 0;
                    }
                    else
                    {
                        audioSource.PlayOneShot(walkingSound);
                        numberOfWalkingSound++;
                    }

                }
                var tmp = Vector3.MoveTowards(this.transform.position, Player.transform.position, step);
                var height = Terrain.activeTerrain.SampleHeight(transform.position) + Terrain.activeTerrain.transform.position.y;
                tmp.y = height;
                transform.position = tmp;
            }
            else
            {
                anim.SetFloat(speedHash, 0);
                anim.SetBool(attackHash, true);

                anim.Play(attackStateHash);
                anim.speed = attackSpeed;


                if (Time.time > nextActionTime)
                {
                    nextActionTime = Time.time + deltaTimeHitByZombie;
                    HitByZombie();

                    audioSource.clip = attackSound;
                    audioSource.Play();
                    //StopCoroutine("FlashWhenHit");
                }

            }

            var targetRotation = Quaternion.LookRotation(Player.transform.position - transform.position);
            var str = Mathf.Min(walkingSpeed * Time.deltaTime, 1);
            transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, str);
        }
    }

    private void HitByZombie()
    {
        // Damage
        Player.SendMessageUpwards("ChangeHealth", -damage, SendMessageOptions.DontRequireReceiver);

        //Make red screen
        StartCoroutine(gameController.FlashWhenHit(damageImage));
    }

    public void DieZombie()
    {
        //anim.SetBool(attackHash, false);
        //anim.SetFloat(speedHash, 0);
        //anim.SetBool(isDeadHash, true);
        //isDead = true;
        //audioSource.PlayOneShot(fallbackSound);
        //Destroy(this.gameObject, 3.0f);
        gameController.SetTransparentWhenZombieDie(damageImage);
        Destroy(this.gameObject, 0.1f);
        StatsGame.Cash +=  WorthOfZombie;
    }

    public void HitbyPlayer()
    {
        audioSource.PlayOneShot(bloodSound);
    }
}
